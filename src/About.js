import React from 'react';
import { Route, Link } from 'react-router-dom';
import FAQ from './FAQ';

export default ({ history, location, match }) => {
  console.log({ history, location, match });
  // alert(match.url)
  return (
    <div>
      Hey from the about page
      <br />
      <Link to="/contact">Go to contact page</Link>
      <br />
      <Link to={`${match.url}/faq`}>Go to FAQ</Link>
      <br />
      <Link to={`${match.url}/location`}>Go to location</Link>
      <br />

      <Route exact path={`${match.url}/faq`} component={FAQ} />
      <Route path={`${match.url}/location`} render={() => <div> imagine this is a map </div>} />
    </div>
  )
}