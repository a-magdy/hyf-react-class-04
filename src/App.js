import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import About from './About';
import FAQ from './FAQ';
import Contact from './Contact';
import Home from './Home';
import Users from './Users';
import UserDetails from './UserDetails';

class App extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/jsx" component={About} />
        {/* <Route path="/about/faq" component={FAQ} /> */}
        <Route path="/contact" component={Contact} />
        <Route path="/404" render={() => <div>Not found!</div>} />

        <Route path="/users" component={Users} />
        <Route path="/user/:id" component={UserDetails} />
        {/* <Route path="/children-test" children={() => <div>Children test</div>} /> */}
      </div>
    );
  }
}

export default App;
